<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/management.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面</title>
</head>
<body>


	<div class="title">
		ユーザー管理画面
	</div>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="header">
			<div class = link>
			<a href="signup">新規登録</a>
			<a href="./">戻る</a>
			</div>
			
			<table class="users">
			<tr>
				<th>ログインID</th>
				<th>名前</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>復活・停止</th>
				<th>編集</th>
			</tr>
				<c:forEach items="${users}" var="user">
					
					<c:if test="${user.is_deleted == 1 }">
					
					<tr style="color:red;background:#000000">
					
					</c:if>
					
					<c:if test="${user.is_deleted != 1 }">
					<tr style="background:#ffffff">
					</c:if>
					
						<td><c:out value="${user.login_id}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.branch_name}" /></td>
						<td><c:out value="${user.position_name}" /></td>
						<td>
							
							<c:if test="${loginUser.id == user.id}">
								<div class = "loginPara">
									<c:out value="ログイン中" />
								</div>
							</c:if>
							
							<c:if test="${loginUser.id != user.id}">
								<div class = "parameter">
									<c:if test="${user.is_deleted == 0 }">
										<form action="management" method="post">
											<input type="hidden" name="id" value="${user.id}" />
											<input type="hidden" name="flag" value="1" />
											<input type="submit" value="停止" onClick="return check();">
										</form>
									</c:if>
								
									<c:if test="${user.is_deleted == 1 }">
										
										<form action="management" method="post">
										
											<input type="hidden" name="id" value="${user.id}" />
											<input type="hidden" name="flag" value="0" />
											<input type="submit" value="復活" onClick="return check();">
										
										</form>
										
									</c:if>
								</div>
							</c:if>
						</td>
						<td>
							<div class = "edit" >
								<form action="setting" method="get">
									<input type="hidden" name="id" value="${user.id}" />
									<input type="submit" value="編集">
								</form>
							</div>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<jsp:include page="Copyright.jsp" />
	
	<script type="text/javascript">
		function check() {
		// 「OK」ボタン押下時
			if (confirm('実行しますか？')){
				alert('実行しました');
				return true;
			}else {
				return false;
			}
		}
	</script>

</body>
</html>