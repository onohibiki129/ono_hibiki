<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/NewContribution.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="header">
	新規投稿
</div>



	<div class="form-area">
		<div class="link">
			<a href="./">ホーム</a>
			<a href="./">戻る</a>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<form action="newContribution" method="post">
		
		<ul id="tooltip">
		
		<li data-text="30文字まで">
			タイトル<br>
			<input name="title" value="${contribution.title}" id="title"/>
		</li>
		
		<li data-text="10文字まで">
			カテゴリー <br>
			<input name="category" value="${contribution.category}" id="category"/>
		</li>
		
		<li data-text="1000文字まで">
			本文
			<br>
			<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${contribution.text}" /></textarea>
		</li>
		
			
			
		</ul>
			<input type="submit" value="送信">
			<br>
		</form>
			<c:remove var="contribution" scope="session"/>
	</div>

<script>
  var obj = document.getElementById("tooltip").getElementsByTagName("li");
  var length = obj.length;
  for(var i = 0; i < length;i++) {
	  obj.item(i).onmouseover = function () {
		var element = document.createElement("div");
		element.innerHTML = this.getAttribute('data-text');
		element.className = "tooltips";
		this.appendChild(element);
	  }
	  obj.item(i).onmouseout = function () {
		this.removeChild(this.childNodes.item(this.childNodes.length - 1)); 
	  }
  }
</script>
	
</body>
<jsp:include page="Copyright.jsp" />
</html>