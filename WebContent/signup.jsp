<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/setting.css" rel="stylesheet" type="text/css">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">
	
		<div class="header">
			ユーザー新規登録
		</div>
		<div class = link>
			<a href="./">ホーム</a>
			<a href="management">戻る</a>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />
			
			<label for="login_id">ログインID</label>
			<br>
			<input name="login_id" value="${editUser.login_id}" id="login_id"/>
			
			<br />
			
			<label for="password">パスワード</label>
			<br>
			<input name="password" type="password" id="password" />
			
			<br />
			
			<label for="passwordConfirm">パスワード（確認）</label>
			<br>
			<input type="password" name="passwordConfirm" id="passwordConfirm">
			
			<br>
			
			<label for="name">名前</label>
			<br>
			<input name="name" value="${editUser.name}" />
			
			<br>
			

			<label for="branch_id">配属店</label>
			<br>
			<select name="branch_id">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}">${branch.name}</option>
					<c:if test="${editUser.branch_id == branch.id}">
						<option value="${branch.id}"selected>${branch.name}</option>
					</c:if>
				</c:forEach>
			</select>
		
			<br />
		
			<label for="position_id">部署・役職</label>
			<br>
			<select name="position_id">
				<c:forEach items="${positions}" var="position">
					<option value="${position.id}">${position.name}</option>
					<c:if test="${editUser.position_id == position.id}">
						<option value="${position.id}"selected>${position.name}</option>
					</c:if>
				</c:forEach>
			</select>
			<br />
			<input type="submit" value="登録" />
			<br>
			
		</form>
		
	</div>
	<jsp:include page="Copyright.jsp" />
</body>
</html>