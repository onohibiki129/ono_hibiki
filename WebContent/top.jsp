<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" >

<link href="./css/topstyle.css" rel="stylesheet" type="text/css">
<body>

	
		<div id="header">
			社内掲示板
		</div>
		
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			
		<jsp:include page="menu.jsp" />
		
		<div class = content>
		
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			
			<form action="./" method="get">
				<br />
				<label for="category">カテゴリー検索</label>
				<input name="category" value="${category}" />
				<label for="searchDate">期間検索</label>
				<input name="startDate" value="${startDate}" id="datepicker" />
				～
				<input name="endDate" value="${endDate}" id="datepickerEnd"/>
				<input type="submit" value="検索">
				<br />
			</form>

			
			
			<div class="contributions">
				<c:forEach items="${contributions}" var="contribution">
					<div class="box2">
					<dl id="acMenu">
						<dt>
							<div class="title">
								<c:out value="${contribution.title}" />
							</div>
						</dt>
						<dd>
						<div class="category">
							カテゴリー：
							<c:out value="${contribution.category}" />
						</div>
						<div class="text">
							<c:forEach var="s" items="${fn:split(contribution.text, '
								')}">
								<c:out value="${s}" />
								<br>
							</c:forEach>
						</div>
						<br>
						<div class="date">
							<fmt:formatDate value="${contribution.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<div class="name">
							投稿者：
							<c:out value="${contribution.name}" />
						</div>
						<c:if test="${loginUser.id == contribution.user_id }">
							<form action="./" method="post">
								<input type="hidden" name="id" value="${contribution.id}" />
								<input type="hidden" name="deleteFlag" value="0" />
								<input type="submit" value="削除" onClick="return check();">
							</form>
						</c:if>
						<br>
						<br>

						<div class="comment">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${contribution.id == comment.contribution_id}" >
									<div class="comment"></div>
									<div class="text">
										<c:forEach var="s" items="${fn:split(comment.text, '
											')}">
											<c:out value="${s}" />
											<br>
										</c:forEach>
									</div>
									<div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
									<div class="name"><c:out value="${comment.name}" /></div>
									
									<c:if test="${loginUser.id == comment.user_id }">
										<form action="./" method="post">
											<input type="hidden" name="id" value="${comment.id}" />
											<input type="hidden" name="deleteFlag" value="2" />
											<input type="submit" value="削除" onClick="return check();">
										</form>
									</c:if>
								</c:if>
							</c:forEach>
						</div>
					
						<div class="form-area">
							<c:if test="${ isShowMessageForm }">
								<form action="./" method="post">
									コメント (500文字まで)
									<br />
									
									
									<textarea name="comment" cols="100" rows="5" id="comment"></textarea>
									<span class="count">0</span>
									
									<input type="hidden" name="contribution_id" value="${contribution.id}" />
									<input type="hidden" name="deleteFlag" value="1" />
									<input type="submit" value="投稿">
									
								</form>
							</c:if>
						</div>
						</dd>
						</dl>
					</div>
				</c:forEach>
			</div>
		</div>

		<jsp:include page="Copyright.jsp" />

	
	<script>
	var $d = jQuery.noConflict();
		$d(function() {
			$d("#datepicker").datepicker();
		});
	</script>
	
	<script>
	var $e = jQuery.noConflict();
		$e(function() {
			$e("#datepickerEnd").datepicker();
		});
	</script>

	<script type="text/javascript">
		function check() {
		// 「OK」ボタン押下時
			if (confirm('削除しますか？')){
				alert('削除しました');
				return true;
			}else {
				return false;
			}
		}
	</script>
	
	
	
	<script>
	var $c = jQuery.noConflict();
	$c(document).ready(function(){
		$c('#comment').inputColorShift({
			startColor:'#00ffff',
			endColor:'#ff0000',
			maxlength: 40
		});
	});
</script>

<script>
var $a = jQuery.noConflict();
	$a(function(){
		$a("#acMenu dt").on("click", function() {
			$a(this).next().slideToggle();
		});
	});
</script>

<script>
var $s = jQuery.noConflict();
	$s(function(){
		$s("#SearchMenu dt").on("click", function() {
			$s(this).next().slideToggle();
		});
	});
</script>

<script type="text/javascript">
var $t = jQuery.noConflict();
$t(function(){

    var text_max = 500; // 最大入力値
    $t(".count").text(text_max - $t("#comment").val().length);

    $t("#comment").on("keydown keyup keypress change",function(){
        var text_length = $t(this).val().length;
        var countdown = text_max - text_length;
        $t(".count").text(countdown);
        // CSSは任意で
        if(countdown < 0){
            $t('.count').css({
                color:'#ff0000',
                fontWeight:'bold'
            });
        } else {
            $t('.count').css({
                color:'#000000',
                fontWeight:'normal'
            });
        }
    });
});
</script>
	
</body>
</html>
