<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/login.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<div class="box-title">ログイン</div>
		<div class="contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
			<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
			</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	
			<form action="login" method="post">
				<br />
				<label for="accountOrEmail">ログインID</label><br>
				<input name="login_id" value="${login_id}" />
				
				<br />
				
				<label for="password">パスワード</label><br>
				<input name="password" type="password"id="password" />
				
				<br />
					
				<div class="loginLink">
					<input type="submit" value="ログイン" /><br>
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="Copyright.jsp" />
</body>
</html>