package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns={"/management","/signUp","/setting"})
public class ManagementFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)req).getSession();
		List<String> messages = new ArrayList<String>();

		User user = (User)session.getAttribute("loginUser");

		if(user.getBranch_id().equals("1")){
			chain.doFilter(req, res);
		}else{
			messages.add("アクセス権の無いユーザーです");
			session.setAttribute("errorMessages", messages);
			RequestDispatcher dispatcher = req.getRequestDispatcher("/index.jsp");
			dispatcher.forward(req,res);
		}
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}