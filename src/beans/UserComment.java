package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int contribution_id;
	private String text;
	private int user_id;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private Date created_date;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContribution_id() {
		return contribution_id;
	}

	public void setContribution_id(int contribution_id) {
		this.contribution_id = contribution_id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}