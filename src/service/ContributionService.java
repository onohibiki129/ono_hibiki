package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import beans.UserContribution;
import dao.ContributionDao;
import dao.UserContributionDao;

public class ContributionService {

	public void register(Contribution contribution) {

		Connection connection = null;
		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.insert(connection, contribution);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserContribution> getContribution(String category,String startDate,String endDate) {

		//デフォルトで1年前にする
		if(StringUtils.isEmpty(startDate)){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			calendar.add(Calendar.YEAR, -1);
			startDate=sdf.format(calendar.getTime());
		}

		//デフォルトで現在にする
		if(StringUtils.isBlank(endDate)==true){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			calendar.add(Calendar.DATE, +1);
			endDate=sdf.format(calendar.getTime());
		}

		Connection connection = null;
		try {
			connection = getConnection();

			UserContributionDao contributionDao = new UserContributionDao();
			List<UserContribution> ret = contributionDao.getUserContributions(connection, LIMIT_NUM, category,startDate,endDate);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}






