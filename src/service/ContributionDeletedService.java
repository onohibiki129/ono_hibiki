package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.ContributionDao;

public class ContributionDeletedService {

	public void register(String id) {

		Connection connection = null;
		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.delete(connection, id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
