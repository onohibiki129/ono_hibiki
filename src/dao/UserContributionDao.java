package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserContribution;
import exception.SQLRuntimeException;

public class UserContributionDao {

	//掲示板取り込み
	public List<UserContribution> getUserContributions(Connection connection, int num, String category, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("contributions.id as id, ");
			sql.append("contributions.title as title, ");
			sql.append("contributions.text as text, ");
			sql.append("contributions.category as category, ");
			sql.append("contributions.user_id as user_id, ");  

			sql.append("users.name as name, ");

			sql.append("contributions.created_date as created_date ");

			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");

			//期間の検索
			sql.append("WHERE contributions.created_date >= ? ");

			sql.append("AND contributions.created_date <= ? ");



			//カテゴリー検索欄に入力があれば検索
			if(category!=""){
				sql.append("AND category LIKE ? ");
			}

			sql.append("ORDER BY created_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());

			//ワイルドカードの定義

			ps.setString(1,startDate+"%");
			ps.setString(2,endDate+" 23:59:59");

			if(category!=""){
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserContribution> ret = toUserContributionList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserContribution> toUserContributionList(ResultSet rs)throws SQLException {

		List<UserContribution> ret = new ArrayList<UserContribution>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int user_id = rs.getInt("user_id");
				String name = rs.getString("name");

				Timestamp createdDate = rs.getTimestamp("created_date");

				UserContribution contribution = new UserContribution();
				contribution.setId(id);
				contribution.setTitle(title);
				contribution.setText(text);
				contribution.setCategory(category);
				contribution.setName(name);
				contribution.setUser_id(user_id);
				contribution.setCreated_date(createdDate);

				ret.add(contribution);
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}