package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;


public class CommentDao {

	//コメント投稿
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("contribution_id");
			sql.append(", text");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("  ?");//contribution_id
			sql.append(", ?"); // text
			sql.append(", ?");//user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, comment.getContribution_id());
			ps.setString(2, comment.getText());
			ps.setInt(3, comment.getUser_id());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//コメント削除
	public void delete(Connection connection, String id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments ");
			sql.append("WHERE ");
			sql.append("id=?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1,id );

			ps.executeUpdate();

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}