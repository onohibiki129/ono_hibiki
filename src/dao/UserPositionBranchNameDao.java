package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserName;
import exception.SQLRuntimeException;

public class UserPositionBranchNameDao {

	public List<UserName> getUserNames(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");

			//店舗名と役職名
			sql.append("branches.name as name, ");
			sql.append("positions.name as name, ");

			sql.append("users.is_deleted as is_deleted, ");
			sql.append("users.created_date as created_date ");

			//userDBに各名前を結合
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");

			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");

			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserName> ret = toUserNameList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserName> toUserNameList(ResultSet rs)
			throws SQLException {

		List<UserName> ret = new ArrayList<UserName>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branchesName = rs.getString("branches.name");
				String positionsName = rs.getString("positions.name");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserName user = new UserName();
				user.setId(id);
				user.setLogin_id(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_name(branchesName);
				user.setPosition_name(positionsName);
				user.setIs_deleted(isDeleted);
				user.setCreated_date(createdDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}