package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Contribution;
import exception.SQLRuntimeException;

public class ContributionDao {

	//掲示板投稿
	public void insert(Connection connection, Contribution contribution) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // title
			sql.append(", ?"); // text
			sql.append(", ?");//category
			sql.append(", ?");//user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, contribution.getTitle());
			ps.setString(2, contribution.getText());
			ps.setString(3, contribution.getCategory());
			ps.setInt(4, contribution.getUser_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//掲示板削除
	public void delete(Connection connection, String id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM contributions ");
			sql.append("WHERE ");
			sql.append("id=?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1,id );

			ps.executeUpdate();

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
