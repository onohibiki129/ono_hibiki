package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	//コメント取り込み
	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");

			sql.append("comments.id as id, ");
			sql.append("comments.contribution_id as contribution_id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");

			sql.append("users.name as name, ");

			sql.append("comments.created_date as created_date ");

			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");

			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				int contribution_id = rs.getInt("contribution_id");
				String text = rs.getString("text");
				int user_id = rs.getInt("user_id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setContribution_id(contribution_id);
				comment.setText(text);
				comment.setUser_id(user_id);
				comment.setName(name);
				comment.setCreated_date(createdDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}