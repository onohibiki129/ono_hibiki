package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		User editUser = new UserService().getUser(id);
		request.setAttribute("editUser", editUser);

		List<Branch> branches = new BranchService().getBranches();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("positions", positions);

		if(editUser==null){
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメーターです");

			response.sendRedirect("management");
			session.setAttribute("errorMessages", messages);
		}else {
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}

	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		List<Branch> branches = new BranchService().getBranches();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("positions", positions);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}
			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch_id(request.getParameter("branch_id"));
		editUser.setPosition_id(request.getParameter("position_id"));
		editUser.setIs_deleted(request.getParameter("is_deleted"));

		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		int id = (Integer.parseInt(request.getParameter("id")));
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}else{
			if(!login_id.matches("^[0-9a-zA-Z]+$")) {
				messages.add("ログインIDは半角英数字で入力してください");
			}
			if(login_id.length()<6 || login_id.length()>20) {
				messages.add("ログインIDは6文字以上20文字以内で入力してください");
			}
		}
		if(new UserService().getSettingLoginId(login_id,id)==false){
			messages.add("ログインIDが重複しています");
		}
		if (StringUtils.isEmpty(password) != true) {

			if(!password.matches("^[0-9a-zA-Z]+$")) {
				messages.add("パスワードは半角英数字で入力してください");
			}
			if(password.length()<6 || password.length()>20) {
				messages.add("パスワードは6文字以上20文字以内で入力してください");
			}
		}

		if (!password.equals(passwordConfirm)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if(name.length()>10) {
			messages.add("ユーザーの名前は10文字以下で入力してください");
		}

		// TODO アカウントが既に利用されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
