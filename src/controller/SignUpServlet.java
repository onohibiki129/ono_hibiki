
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String id = request.getParameter("id");
		User user = new UserService().getUser(id);
		request.setAttribute("editUser", user);

		List<Branch> branches = new BranchService().getBranches();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = new User();

		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_id(request.getParameter("branch_id"));
		user.setPosition_id(request.getParameter("position_id"));



		if (isValid(request, messages) == true) {
			new UserService().register(user);
			response.sendRedirect("management");

		} else {
			List<Branch> branches = new BranchService().getBranches();
			request.setAttribute("branches", branches);
			List<Position> positions = new PositionService().getPositions();
			request.setAttribute("positions", positions);

			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordConfirm = request.getParameter("passwordConfirm");

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}else{
			if(!login_id.matches("^[0-9a-zA-Z]+$")) {
				messages.add("ログインIDは半角英数字で入力してください");
			}
			if(login_id.length()<6 || login_id.length()>20) {
				messages.add("ログインIDは6文字以上20文字以内で入力してください");
			}
		}
		if(new UserService().getSignUpId(login_id)==false){
			messages.add("ログインIDが重複しています");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else{
			if(!password.matches("^[0-9a-zA-Z]+$")) {
				messages.add("パスワードは半角英数字で入力してください");
			}
			if(password.length()<6 || password.length()>20) {
				messages.add("パスワードは6文字以上20文字以内で入力してください");
			}
		}
		if (!password.equals(passwordConfirm)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if(name.length()>10) {
			messages.add("ユーザーの名前は10文字以下で入力してください");
		}
		// TODO アカウントが既に利用されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}