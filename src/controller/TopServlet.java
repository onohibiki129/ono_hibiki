package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserComment;
import beans.UserContribution;
import service.CommentDeleteService;
import service.CommentService;
import service.ContributionDeletedService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

		//ログイン状態で表示

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		String category = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		//カテゴリー検索欄がnullならブランクをデフォルトにする
		if(category==null){
			category="";
		}

		if(StringUtils.isEmpty(startDate)){
			startDate="";
		}

		if(StringUtils.isEmpty(endDate)){
			endDate="";
		}


		//呼び出したデータベースをリスト化して一覧表示
		List<UserContribution> messages = new ContributionService().getContribution(category,startDate,endDate);

		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("contributions", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("category",category );
		request.setAttribute("startDate",startDate );
		request.setAttribute("endDate",endDate );
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		String deleteFlag = request.getParameter("deleteFlag");

		//ボタンにフラグを持たせ複数のボタンを同じポストに送れるようにする

		//掲示板削除
		if(deleteFlag.equals("0")){
			String id = request.getParameter("id");
			new ContributionDeletedService().register(id);
			response.sendRedirect("./");

			//コメントする
		}else if(deleteFlag.equals("1")){
			HttpSession session = request.getSession();

			List<String> messages = new ArrayList<String>();

			if (isValid(request, messages, response) == true) {

				User user = (User) session.getAttribute("loginUser");

				Comment comment = new Comment();
				comment.setContribution_id(Integer.parseInt(request.getParameter("contribution_id")));
				comment.setText(request.getParameter("comment"));
				comment.setUser_id(user.getId());

				new CommentService().register(comment);
				response.sendRedirect("./");

			} else {
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
			}

			//コメント削除	
		}else if(deleteFlag.equals("2")){
			String id = request.getParameter("id");
			new CommentDeleteService().register(id);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages,HttpServletResponse response)  throws IOException, ServletException {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true) {
			messages.add("コメントを入力してください");
		}
		if (500< comment.length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}


